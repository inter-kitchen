const Recipe    = require('../models/recipe'),
    User        = require('../models/user'),
    Account     = require('../models/account'),
    Step        = require('../models/step');

const middlewareObj = {};

middlewareObj.checkRecipeOwnership = (req, res, next) => {
    if(req.isAuthenticated()) {
        Recipe.findById(req.params.id, (err, foundRecipe) => {
            if(err) {
                console.log(err);
            } else {
                if(!foundRecipe) {
                    console.log(`Recipe not found!`);
                    return res.redirect('back');
                }
                if(foundRecipe.author.id.equals(req.user._id)) {
                    next();
                } else {
                    console.log(`You don't own this...`);
                    res.redirect('back');
                }
            }
        });
    } else {
        console.log(`Need to be logged in`);
        res.redirect('/');
    }
}

middlewareObj.checkStepOwnership = (req, res, next) => {
    if(req.isAuthenticated()) {
        Recipe.findById(req.params.id, (err, foundRecipe) => {
            if(err) {
                console.log(err);
            } else {
                if(!foundRecipe) {
                    console.log(`Recipe not found!`);
                    return res.redirect('back');
                }
                Step.findById(req.params.sid, (err, foundStep) => {
                    if(err) {
                        console.log(err);
                    } else {
                        if(foundRecipe.steps.includes(foundStep._id)) {
                            next();
                        } else {
                            console.log(`You don't own this...`);
                            res.redirect('back');
                        }
                    }
                });
            }
        });
    } else {
        console.log(`Need to be logged in`);
        res.redirect('/');
    }
}

middlewareObj.isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    //req.flash("error", "You need to be logged in to do that!"); // Must come before redirecting
    console.log(`Need to be logged in`);
    res.redirect("/login");
};

middlewareObj.checkAccountOwnership = (req, res, next) => {
    if(req.isAuthenticated()) {
        let ath = {
            id: req.user._id,
            username: req.user.username
        }
        Account.findOne({ author: ath }, (err, foundAccount) => {
            if(err) {
                console.log(err);
            } else {
                console.log(foundAccount.author.id)
                console.log(req.user._id);
                if(foundAccount.author.id.equals(req.user._id)) {
                    next();
                } else {
                    console.log(`Please login to "your" account`);
                    return res.redirect(`back`);
                }
            }
        });
    } else {
        console.log(`Need to be logged in...`);
        res.redirect(`/`);
    }
};

// Get All Categories
middlewareObj.getCats = (req, res, next) => {
    Recipe.find({}, (err, Recipes) => {
        if(err) {
            console.log(err);
        } else {
            const cats = [];
            Recipes.map(r => {
                if(!(cats.includes(r.category))) {
                    cats.push(r.category);
                }
            });
            const recipes = Recipes
            req.recipes = recipes;
            req.cats = cats;
            return next();
        }
    }); 
};

// Get Popular Recipes
middlewareObj.getPops = (req, res, next) => {
    Recipe.find({}, (err, Recipes) => {
        if(err) {
            console.log(err);
        } else {
            const lmPop = Recipes.slice(0, 5);
            lmPop.sort((a, b) => b.isLiked - a.isLiked);
            req.lmPop = lmPop;
            return next();
        }
    });
};

// Get User
middlewareObj.getUser = (req, res, next) => {
    User.findById(req.params.id, (err, user) => {
        if(err) {
            console.log(err);
            res.redirect(`back`);
        } else {
            const daUser = user;
            req.daUser = daUser;
            return next();
        }
    })
};

// Get Account 
middlewareObj.getAccount = (req, res, next) => {
    if(req.user) {
        let ath = {
            id: req.user._id,
            username: req.user.username
        }
        Account.findOne({ author: ath }, (err, foundAccount) => {
            if(err) {
                console.log(err);
            } else {
                const daAccount = foundAccount;
                req.daAccount = daAccount;
                return next();
            }
        });       
    } else {
        return next();
    }
};

module.exports = middlewareObj;
