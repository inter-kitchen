$('#srchBtn').on('click', (e) => {
    e.preventDefault();
    const q = $('#srchInpt').val();
    $('#searchForm').attr('action', `/search/${q}`);
    $('#searchForm').submit();
});