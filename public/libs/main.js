$('#plus').on('click', (event) => {
    const newVal = parseInt(document.querySelector('#stpnum').value) + 1;
    document.querySelector('#stpnum').value = newVal;
    const step = newVal;
    $('#sub').before(`
        <div id='added${step}' class='adds'>
            <label id='added${step}' for="">Step Number:</label>
            <input id='added${step}' name='step${step}[number]' type='text' value='${step}'>
            <label id='added${step}' for="">Step instructions:</label>
            <textarea id='added${step}' name='step${step}[step]'></textarea>
        </div>
    `)
});

$('#minus').on('click', (event) => {
    const newVal = parseInt(document.querySelector('#stpnum').value) - 1;
    document.querySelector('#stpnum').value = newVal;
    const step = newVal;
    const toRm = step + 1;
    for(i = 0; i < 7; i++) {
        $(`#added${toRm}`).remove();
    }
});
