$('#plus').on('click', (event) => {
    const newVal = parseInt(document.querySelector('#stpnum').value) + 1;
    document.querySelector('#stpnum').value = newVal;
    const step = newVal;
    $('#sub').before(`
        <label id='added${step}' for="">Step Number:</label>
        <input id='added${step}' name='step${step}[number]' type='text' value='${step}'>
        <label id='added${step}' for="">Step photo:</label>
        <input id='added${step}' name='step${step}[photo]' type="text">
        <label id='added${step}' for="">Step instructions:</label>
        <input id='added${step}' name='step${step}[step]' type="text">
    `)
});

$('#minus').on('click', (event) => {
    const newVal = parseInt(document.querySelector('#stpnum').value) - 1;
    document.querySelector('#stpnum').value = newVal;
    const step = newVal;
    const toRm = step + 1;
    for(i = 0; i < 6; i++) {
        $(`#added${toRm}`).remove();
    }
});