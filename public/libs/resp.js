if($(window).width() < 800) {
    $('.cats').remove();
    $('.pops').remove();
    $('.sets').remove();
    $('.bio').remove();
    
    if(window.location.href.indexOf('account') > -1) {
        if($('.magicSets').length) {
            $('.content').after(`
                <div class='bottomNav'>
                <button id='bNavSets'>Settings</button>
                <button id='bNavBio'>Details</button>
                </div>
            `)
        } else {
            $('.content').after(`
                <div class='bottomNav'>
                <button id='bNavCats'>Categories</button>
                <button id='bNavBio'>Details</button>
                </div>
            `)
        }
    } else {
        $('.content').after(`
            <div class='bottomNav'>
            <button id='bNavCats'>Categories</button>
            <button id='bNavPops'>Popular</button>
            </div>
        `)
    }

}

$('#bNavCats').on('click', (event) => {
    $(`.magicCats`).toggleClass('invis');
    if(!($(`.magicPops`).hasClass('invis'))) {
        $(`.magicPops`).toggleClass(`invis`);
    }
});

$('#bNavPops').on('click', (event) => {
    $(`.magicPops`).toggleClass('invis');
    if(!($(`.magicCats`).hasClass(`invis`))) {
        $(`.magicCats`).toggleClass(`invis`);
    }
});

$('#bNavSets').on('click', (event) => {
    $(`.magicSets`).toggleClass(`invis`);
    if(!($(`.magicPops`).hasClass(`invis`))) {
        $(`.magicPops`).toggleClass(`invis`);
    }
});

$('#bNavBio').on('click', (event) => {
    $(`.magicPops`).toggleClass(`invis`);
    if(!($(`.magicSets`).hasClass(`invis`))) {
        $(`.magicSets`).toggleClass(`invis`);
    }
});

