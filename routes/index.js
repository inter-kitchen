const express       = require('express'),
        router      = express.Router(),
        fs          = require('fs'),
        User        = require("../models/user"),
        Account     = require("../models/account"),
        Recipe      = require('../models/recipe'),
        middleware  = require('../middleware'),
        passport    = require('passport');

// INDEX 
router.get('/', middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'home';
    res.render('index', { 
        page: page, 
        usr: req.user,
        popular: req.lmPop, 
        cats: req.cats 
    });
});

// LOGIN 
router.get('/login', middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'login';
    res.render('login', { 
        page: page, 
        usr: req.user,
        popular: req.lmPop,
        cats: req.cats 
    });
});

router.post(
    "/login",
    passport.authenticate("local", {
        successRedirect: "/",
        failureRedirect: "/login",
    }),
    (req, res) => {}
);

// REGISTER
router.get('/register', middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'register';
    res.render('register', { 
        page: page, 
        usr: req.user,
        popular: req.lmPop,
        cats: req.cats 
    });
});

router.post("/register", (req, res) => {
    let newUser = new User({ username: req.body.username });
    User.register(newUser, req.body.password, (err, user) => {
        if (err) {
            console.log(err);
            req.flash("error", err.message);
            return res.redirect("/register");
        }
        passport.authenticate("local")(req, res, () => {
            Account.create(req.body.account, (err, newAccount) => {
                if (err) {
                    console.log(err);
                    req.flash("error", err.message);
                } else {
                    const dpath = `/var/www/inter-kitchen/public/images/${req.user.username}`;
                    fs.mkdir(dpath, (err) => {
                        if(err) throw err;
                        console.log(`Created directory ${dpath}`);
                    })

                    newAccount.author.id        = user._id;
                    newAccount.author.username  = user.username;
                    newAccount.bio.name         = req.body.name;
                    newAccount.bio.website      = req.body.website;
                    newAccount.bio.about        = req.body.about;
                    newAccount.save();
                    req.flash("success", "Welcome " + user.username);
                    res.redirect("/");
                }
            });
        });
    });
});

// LOGOUT 
router.get("/logout", (req, res) => {
    req.logout();
    req.flash("success", "Logged out");
    res.redirect("/");
});

module.exports = router;
