const express = require('express'),
    router      = express.Router({ mergeParams: true }),
    fs          = require('fs'),
    middleware  = require('../middleware'),
    Recipe      = require('../models/recipe'),
    Step        = require('../models/step'),
    User        = require('../models/user'),
    Account     = require('../models/account');

// SHOW
router.get('/:id', middleware.getUser, middleware.getCats, (req, res) => {
    let at = {
        id: req.daUser._id,
        username: req.daUser.username
    }
    Account.findOne({ author: at }, (err, foundAccount) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log('found account: ', foundAccount);
            let ath = {
                id: foundAccount.author.id,
                username: foundAccount.author.username
            }
            Recipe.find({ author: ath }, (err, foundRecipes) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log('found recipes: ', foundRecipes);
                    const page = 'account';
                    res.render('account/account', { 
                        page: page, 
                        usr: req.user, 
                        account: foundAccount,
                        cats: req.cats, 
                        recipes: foundRecipes 
                    });
                }
            });
        } 
    });
});

//EDIT BIO PAGE
router.get('/:id/bio/edit', middleware.checkAccountOwnership, (req, res) => {
    let ath = {
        id: req.user._id,
        username: req.user.username
    }
    Account.findOne({ author: ath }, (err, foundAccount) => {
        if(err) {
            console.log(err);
        } else {
            const page = 'account';
            res.render('account/editBio', {
                page: page,
                usr: req.user,
                account: foundAccount
            });
        }
    });
});

// UPDATE
router.put('/:id', middleware.checkAccountOwnership, (req, res) => {
    let ath = {
        id: req.user._id,
        username: req.user.username
    }
    Account.findOne({ author: ath }, (err, foundAccount) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            foundAccount.bio.name = req.body.bio.name;
            foundAccount.bio.website = req.body.bio.website;
            foundAccount.bio.about = req.body.bio.about;
            foundAccount.save();
            res.redirect(`/account/${req.params.id}`);
        }
    });
});

// SAVED RECIPES
router.get(`/:id/saved`, middleware.checkAccountOwnership, (req, res) => {
    let ath = {
        id: req.user._id,
        username: req.user.username
    }
    Account.findOne({ author: ath }).populate('favorites').exec((err, foundAccount) => {
        if(err) {
            console.log(err);
        } else {
            const page ='account';
            res.render(`account/saved`, {
                page: page,
                usr: req.user,
                recipes: foundAccount.favorites,
                account: foundAccount
            });
        }
    })
});

//CHANGE PASSWORD
router.get(`/:id/changepass`, middleware.checkAccountOwnership, (req, res) => {
    let ath = {
        id: req.user._id,
        username: req.user.username
    }
    Account.findOne({ author: ath }, (err, foundAccount) => {
        if(err) {
            console.log(err);
        } else {
            const page = 'account'
            res.render(`account/changepass`, {
                page: page,
                usr: req.user,
                account: foundAccount
            }) 
        }
    });
});

router.post(`/:id/changepass`, middleware.checkAccountOwnership, (req, res) => {
    User.findById(req.params.id, (err, foundUser) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            foundUser.changePassword(req.body.oldpassword, req.body.newpassword, (err) => {
                if(err) {
                    console.log(`Incorrect password!`);
                    req.flash("error", err.message);
                    res.redirect('back');
                } else {
                    console.log(`Password was changed!`);
                    req.flash("success", `Password was changed`);
                    res.redirect(`/account/${req.params.id}`);
                }
            });
        }
    });
});

//DELETE ACCOUNT/USER
router.delete(`/:id`, middleware.checkAccountOwnership, (req, res) => {
    let ath = {
        id: req.user._id,
        username: req.user.username
    }
    Account.findOneAndDelete({ author: ath }, (err) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log('Deleted account...');
        }
    });
    User.findByIdAndDelete(req.user._id, (err) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log(`Deleted user...`);
            req.flash("success", `Deleted user...`);

        }
    });
    const userDir = `/var/www/inter-kitchen/public/images/${req.user.username}`;
    fs.rmdir(userDir, { recursive: true }, (err) => {
        if(err) console.log(err);
        console.log(`Deleted ${userDir}`);
    });
    res.redirect(`/`);
});

module.exports = router;
