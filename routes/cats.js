const express = require('express'),
    router      = express.Router({ mergeParams: true }),
    Recipe      = require('../models/recipe'),
    Step        = require('../models/step'),
    Account     = require('../models/account'),
    middleware  = require('../middleware');

router.get('/:name', middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'home'
    res.render('category/cats', {
        usr: req.user,
        page: page,
        recipes: req.recipes,
        popular: req.lmPop,
        cats: req.cats,
        query: req.params.name
    })
});

module.exports = router;
