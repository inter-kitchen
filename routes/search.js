const express       = require('express'),
        router      = express.Router(),
        User        = require("../models/user"),
        Account     = require("../models/account"),
        Recipe      = require('../models/recipe'),
        middleware  = require('../middleware');

// SEARCH PAGE
router.get('/:query', middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'search';
    res.render('search', {
        usr: req.user,
        page: page,
        recipes: req.recipes,
        popular: req.lmPop,
        cats: req.cats,
        query: req.params.query
    })
});

module.exports = router;
