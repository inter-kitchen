const express       = require('express'),
        router      = express.Router({ mergeParams: true }),
        fs          = require('fs'),
        busboy      = require('connect-busboy'),
        middleware  = require('../middleware'),
        Account     = require('../models/account'),
        Recipe      = require('../models/recipe'),
        Step        = require('../models/step');

// CREATE PAGE
router.get('/create', middleware.isLoggedIn, middleware.getCats, middleware.getPops, (req, res) => {
    const dpath = `/var/www/inter-kitchen/public/images/${req.user.username}`;
    fs.readdir(dpath, (err, files) => {
        if(err) throw err;
        const page = 'create';
        res.render('recipe/create', { 
            page: page, 
            usr: req.user,
            popular: req.lmPop,
            cats: req.cats, 
            files: files
        });
    });
});

//Create
router.post('/', middleware.isLoggedIn, (req, res) => {
    let author = {
        id: req.user._id,
        username: req.user.username
    }
    req.body.recipe.author = author;
    Recipe.create(req.body.recipe, (err, newRecipe) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('/recipe/create');
        } else {
            newRecipe.isLiked = 0;
            newRecipe.save();
            console.log('Created recipe: ', newRecipe);
        }
    });
    const maxSteps = [
        req.body.step1, req.body.step2, req.body.step3,
        req.body.step4, req.body.step5, req.body.step6,
        req.body.step7, req.body.step8, req.body.step9,
        req.body.step10, req.body.step11, req.body.step12,
        req.body.step13, req.body.step14, req.body.step15,
        req.body.step16, req.body.step17, req.body.step18,
        req.body.step19, req.body.step20
    ];
    for( i = 0; i <= req.body.recipe.nsteps - 1; i++ ) {
        Step.create(maxSteps[i], (err, newStep) => {
            if(err) {
                console.log(err);
                req.flash("error", err.message);
                res.redirect('/recipe/create');
            } else {
                console.log('Created step: ', newStep);
                Recipe.findOne({title: req.body.recipe.title}, (err, foundRecipe) => {
                    if(err) {
                        console.log(err);
                        req.flash("error", err.message);
                        res.redirect('back');
                    } else {
                        foundRecipe.steps.push(newStep);
                        foundRecipe.save();
                        console.log('Full recipe: ', foundRecipe);
                    }
                });
            }
        });
    }
    res.redirect(`/`);
});

// SHOW UPLOAD
router.get(`/image/upload`, middleware.getCats, middleware.getPops, (req, res) => {
    const page = 'create';
    res.render(`recipe/upload`, {
        page: page,
        usr: req.user,
        popular: req.lmPop,
        cats: req.cats 
    });
});

// UPLOAD
router.post(`/image`, middleware.isLoggedIn, (req, res) => {
    let fstream;
    req.pipe(req.busboy);
    req.busboy.on('file', (fieldname, file, filename) => {
        console.log(`Uploading ${fieldname}`);
        const home = `/var/www/inter-kitchen/public/images/${req.user.username}/`;
        fstream = fs.createWriteStream(home + filename);
        file.pipe(fstream);
        fstream.on('close', () => {
            console.log(`Finished uploading ${filename}`);
            res.redirect(`/recipe/create`);
        })
    })
});

// SHOW 
router.get('/:id', middleware.getCats, middleware.getPops, middleware.getAccount, (req, res) => {
    Recipe.findById(req.params.id).populate('steps').exec((err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            const splitted = (ing) => { 
                return ing.toString().split(', ');
            }
            const ig = splitted(foundRecipe.ingridients); // Separate ingridients
            foundRecipe.steps.sort((a, b) => a.number - b.number); // Make sure steps are in numerical order
            if(req.user) {
                let recAuthor = {
                    id: foundRecipe.author.id,
                    username: foundRecipe.author.username
                }
                Account.findOne({ author: recAuthor }, (err, foundRecAuthor) => {
                    if(err) {
                        console.log(err);
                    } else {
                        const page = 'show';
                        res.render('recipe/show', { 
                            page: page, 
                            usr: req.user,
                            account: req.daAccount, 
                            recipe: foundRecipe,
                            recAuthor: foundRecAuthor, 
                            ingridients: ig, 
                            popular: req.lmPop,
                            recipes: req.recipes,
                            cats: req.cats 
                        });
                    }
                });
            } else {
                let a = {
                    id: foundRecipe.author.id,
                    username: foundRecipe.author.username
                }
                Account.findOne({ author: a }, (err, foundRecAuthor) => {
                    if(err) {
                        console.log(err);

                    } else {
                        const page = 'show';
                        res.render('recipe/show', { 
                            page: page, 
                            usr: req.user,
                            recAuthor: foundRecAuthor, 
                            recipe: foundRecipe, 
                            ingridients: ig, 
                            popular: req.lmPop,
                            recipes: req.recipes,
                            cats: req.cats 
                        });
                    }
                });
            }
        }
    });
});

// EDIT
router.get('/:id/edit', middleware.checkRecipeOwnership, middleware.getCats, middleware.getPops, (req, res) => {
    Recipe.findById(req.params.id).populate('steps').exec((err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            const dpath = `/var/www/inter-kitchen/public/images/${req.user.username}`;
            fs.readdir(dpath, (err, files) => {
                if(err) throw err;
                const page = 'account'
                foundRecipe.steps.sort((a, b) => a.number - b.number); // Make sure steps are in numerical order
                res.render('recipe/edit', { 
                    page: page, 
                    usr: req.user, 
                    recipe: foundRecipe,
                    recipes: req.recipes, 
                    popular: req.lmPop,
                    cats: req.cats, 
                    files: files
                });
            
            })    
        }
    });
});

// UPDATE
router.put('/:id', middleware.checkRecipeOwnership, (req, res) => {
    Recipe.findByIdAndUpdate(req.params.id, req.body.recipe, (err, updatedRecipe) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log(`Updated Recipe: ${updatedRecipe}`);
            res.redirect(`/recipe/${req.params.id}`);
        }
    });
});

// Delete
router.delete('/:id', middleware.checkRecipeOwnership, (req, res) => {
    Recipe.findByIdAndRemove(req.params.id, (err) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            res.redirect(`/account/${req.user._id}`);
        }
    });
});

// EDIT STEPS
router.get('/:id/step/:sid/edit', middleware.checkStepOwnership, middleware.getCats, middleware.getPops, (req, res) => {
    Recipe.findById(req.params.id, (err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            Step.findById(req.params.sid, (err, foundStep) => {
                if(err) {
                    console.log(err);
                } else {
                    const page = 'account'
                    res.render('recipe/editSteps', { 
                        page: page, 
                        usr: req.user, 
                        step: foundStep, 
                        recipe: foundRecipe,
                        recipes: req.recipes, 
                        popular: req.lmPop,
                        cats: req.cats 
                    });
                }
            });
        }
    });
});

// UPDATE STEPS
router.put('/:id/step/:sid', middleware.checkStepOwnership, (req, res) => {
    Step.findByIdAndUpdate(req.params.sid, req.body.step, (err, updatedStep) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log(`Updated step: ${updatedStep}`);
            res.redirect(`/recipe/${req.params.id}/edit`);
        }
    });
});

// SHOW CREATE STEP 
router.get('/:id/step/create', middleware.checkRecipeOwnership, middleware.getCats, middleware.getPops, (req, res) => {
    Recipe.findById(req.params.id, (err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            const page = 'create';
            res.render('recipe/createStep', { 
                page: page, 
                usr: req.user, 
                recipe: foundRecipe,
                recipes: req.recipes, 
                popular: req.lmPop,
                cats: req.cats 
            });
        }
    });
});

// CREATE A NEW STEP
router.post('/:id/step', middleware.checkRecipeOwnership, (req, res) => {
    Step.create(req.body.newStep, (err, newStep) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            console.log('Created step: ', newStep);
            Recipe.findOne({_id: req.params.id}, (err, foundRecipe) => {
                if(err) {
                    console.log(err);
                } else {
                    foundRecipe.steps.push(newStep);
                    foundRecipe.save();
                    console.log('Full recipe: ', foundRecipe);
                }
            });
        }
        res.redirect(`/recipe/${req.params.id}/edit`);
    });
});

// DELETE STEP
router.delete('/:id/step/:sid', middleware.checkStepOwnership, (req, res) => {
    Step.findByIdAndRemove(req.params.sid, (err) => {
        if(err) {
            console.log(err);
            req.flash("error", err.message);
            res.redirect('back');
        } else {
            res.redirect(`/recipe/${req.params.id}/edit`);
        }
    })
});

// SAVE RECIPE
router.put('/:id/save', middleware.isLoggedIn, (req, res) => {
    Recipe.findById(req.params.id, (err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            let ath = {
                id: req.user._id,
                username: req.user.username
            }
            Account.findOne({ author: ath }, (err, foundAccount) => {
                if(err) {
                    console.log(err);
                } else {
                    if(!(foundRecipe.isLiked)) {
                        foundRecipe.isLiked = 1;
                        foundRecipe.save();
                        foundAccount.favorites.push(foundRecipe);
                        foundAccount.save();
                        res.redirect(`/recipe/${req.params.id}`);
                    } else {
                        foundRecipe.isLiked = foundRecipe.isLiked + 1;
                        foundRecipe.save();
                        foundAccount.favorites.push(foundRecipe);
                        foundAccount.save();
                        res.redirect(`/recipe/${req.params.id}`);
                    }
                }
            });
        }
    });
})

// UNSAVE RECIPE
router.put(`/:id/unsave`, (req, res) => {
    Recipe.findById(req.params.id, (err, foundRecipe) => {
        if(err) {
            console.log(err);
        } else {
            let ath = {
                id: req.user._id,
                username: req.user.username
            }
            Account.findOne({ author: ath }, (err, foundAccount) => {
                if(err) {
                    console.log(err);
                } else {
                    foundRecipe.isLiked = foundRecipe.isLiked - 1;
                    foundRecipe.save();
                    console.log(foundAccount.favorites);
                    const index = foundAccount.favorites.indexOf(req.params.id);
                    if(index > -1) {
                        foundAccount.favorites.splice(index, 1);
                    }
                    console.log(foundAccount.favorites);
                    foundAccount.save();
                    console.log(foundAccount.favorites);
                    res.redirect(`/recipe/${req.params.id}`);
                }
            });
        }
    })
});

module.exports = router;
