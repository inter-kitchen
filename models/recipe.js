const mongoose = require('mongoose');

const recipeSchema = new mongoose.Schema({
    title: String,
    picture: String, 
    category: String,
    ingridients: [String, String],
    isLiked: Number,
    steps: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Step'
        }
    ],
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        username: String
    },
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cmmnt'
        }
    ]
});

module.exports = new mongoose.model('Recipe', recipeSchema);
