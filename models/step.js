const mongoose = require('mongoose');

const stepSchema = new mongoose.Schema({
    number: Number,
    photo: String,
    step: String
});

module.exports = new mongoose.model('Step', stepSchema);