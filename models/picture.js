const mongoose = require('mongoose');

//Schema Setup
const pictureSchema = new mongoose.Schema({
    type: String,
    data: Buffer
});

module.exports = new mongoose.model('Picture', pictureSchema);
