const mongoose = require("mongoose");

const AccountSchema = new mongoose.Schema({
    bio: {
        name: String, 
        about: String, 
        website: String
    },
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
        },
        username: String,
    },
    recipes: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Recipe",
        },
    ],
    favorites: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Recipe",
        },
    ]
});

module.exports = new mongoose.model("Account", AccountSchema);
