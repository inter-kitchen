const bodyParser        = require("body-parser"),
      methodOverride    = require("method-override"),
      mongoose          = require("mongoose"),
      express           = require("express"),
      busboy            = require("connect-busboy"),
      passport          = require("passport"),
      LocalStrategy     = require("passport-local"),
      flash             = require("connect-flash"),
      User              = require("./models/user"),
      app               = express();

// IMPORT ROUTES
const indexRoutes   = require('./routes/index'),
    recipeRoutes    = require('./routes/recipe'),
    accountRoutes   = require('./routes/account'),
    catRoutes       = require('./routes/cats'),
    searchRoutes    = require('./routes/search');

// MongoDB
mongoose.connect('mongodb://USER:PASSWORD@SERVER/DATABASE', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
}).then(() => console.log(`Database connected`))
.catch((err) => console.log(Error, err));

// SETUP
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(methodOverride("_method"));
app.use(busboy());
app.use(flash());

//==========================================================
//                  PASSPORT CONFIG
//==========================================================
app.use(
    require("express-session")({
        secret: "I am gonna be the pirate king",
        resave: false,
        saveUninitialized: false,
    })
);
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
//----------------------------------------------------------

// Current User and Flash Messages
app.use((req, res, next) => {
    res.locals.currentUser  = req.user;
    res.locals.error        = req.flash('error');
    res.locals.success      = req.flash('success');
    next();
});

// Routes
app.use('/', indexRoutes);
app.use('/recipe', recipeRoutes);
app.use('/account', accountRoutes);
app.use('/categories', catRoutes);
app.use('/search', searchRoutes);

// Listen
app.listen(3000, () => {
	console.log('Server started...');
})

//http.createServer(function (req, res) {
//  console.log('received request for url: ' + req.url);
//  res.writeHead(200, {'Content-Type': 'text/html'});
//  res.end();
//}).listen(3000, () => {
//    console.log("Server started...");
//})
